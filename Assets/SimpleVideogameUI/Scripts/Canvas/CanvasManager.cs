﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CanvasManager : MonoBehaviour {

	public Canvas[] canvases;
	public int initialCanvas=0;

	// Use this for initialization
	void Start () {
		ShowCanvas (initialCanvas);
	}


	public void ShowCanvas(int c){
		for (int i= 0; i<canvases.Length; i++) {
			if(i!=c){
				canvases[i].enabled = false;
			}else{
				canvases[i].enabled = true;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
