﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SceneChanger : MonoBehaviour {

	/*
	 * Async values to modify when 
	 * the scene is loading
	 */
	public Slider asyncProgressbar;
	public Text percentageUIText;

	public void goToScene(string name){
		Application.LoadLevel (name);
	}

	public void goToScene(int i){
		Application.LoadLevel (i);
	}

	public IEnumerator goToSceneAsync(string name){
		AsyncOperation asloader = Application.LoadLevelAsync (name);
		
		refreshGraphicElements (asloader.progress);

		yield return asloader;
	}


	public IEnumerator goToSceneAsync(int i){
		AsyncOperation asloader = Application.LoadLevelAsync (i);
		
		refreshGraphicElements (asloader.progress);

		yield return asloader;
	}

	/*
	 * Method that refreshes the passed graphical elements.
	 * Also, this method sends a message OnLoadChanged,
	 * that may be listened by other scripts in the same
	 * gameObject to make other stuff.
	 */
	private void refreshGraphicElements(float progress){
		Debug.Log ("progress: " + progress);

		if (asyncProgressbar) {
			asyncProgressbar.value = progress;
		}
		
		if (percentageUIText) {
			percentageUIText.text = ""+ progress+"%";
		}

		//OnLoadChanged is triggered
		gameObject.SendMessage ("OnLoadChanged",progress);

	}

}
