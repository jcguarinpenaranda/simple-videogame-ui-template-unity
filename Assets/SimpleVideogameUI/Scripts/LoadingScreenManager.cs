﻿using UnityEngine;
using System.Collections;

public class LoadingScreenManager : MonoBehaviour {

	/*
	 * You may pass one of the following
	 * two parameters. If both are passed,
	 * the first one (name) is given priority.
	 */
	public string loadSceneName="";
	public int loadSceneNumber;


	private SceneChanger changer;
	// Use this for initialization
	void Start () {

		changer = GetComponent<SceneChanger> ();

		if (loadSceneName != "") {
			StartCoroutine(changer.goToSceneAsync(loadSceneName));
		} else if (loadSceneNumber!=null) {
			StartCoroutine(changer.goToSceneAsync(loadSceneNumber));
		}

	}

}
