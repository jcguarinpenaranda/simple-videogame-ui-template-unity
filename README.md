# Simple-Videogame-UI
Simple Videogame UI system for Unity 5 with several UIs already programmed and with Google Material Design Icons.

##Icons
Icons taken from Google Material Design icons available at: https://github.com/google/material-design-icons

##Credits
Made By Juan Camilo Guarín P
